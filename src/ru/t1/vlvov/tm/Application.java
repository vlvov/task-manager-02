package ru.t1.vlvov.tm;

import java.util.concurrent.TimeUnit;

import static ru.t1.vlvov.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        if (args == null || args.length == 0) {
            showErrorArgument();
            return;
        }

        final String arg = args[0];

        switch (arg){
            case VERSION:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }

    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Vladimir Lvov");
        System.out.println("E-mail: vlvov@t1.ru");
    }

    private static void showHelp() {
        System.out.printf("%s - show application commands\n", HELP);
        System.out.printf("%s - show application version\n", VERSION);
        System.out.printf("%s - show developer info\n", ABOUT);
    }

    private static void showErrorArgument() {
        System.err.println("Error! This argument not supported");
    }

}
